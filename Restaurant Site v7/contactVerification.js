function checkFields(){
	var popupField = 0;
	var popupText = "";
	var dayCheck = 0;
	
	if (document.getElementById("name").value == ""){
		popupField = 1;
		popupText += "Name field not filled in.\n"
	}
	if (document.getElementById("email").value == "" && document.getElementById("phone").value == ""){
		popupField = 1;
		popupText += "Contact information not filled in.\n"
	}

	if ((document.getElementById("reason").options[document.getElementById("reason").selectedIndex].text) == "Other"){
		if (document.getElementById("additionalInformation").value == ""){
			popupField = 1;
			popupText += "Other was selected and no additional information was filled in.\n"
		}
	}
	if (document.getElementById("mon").checked == 1){
		dayCheck = 1;
	}
	if (document.getElementById("tues").checked == 1){
		dayCheck = 1;
	}
	if (document.getElementById("wed").checked == 1){
		dayCheck = 1;
	}
	if (document.getElementById("thur").checked == 1){
		dayCheck = 1;
	}
	if (document.getElementById("fri").checked == 1){
		dayCheck = 1;
	}
	if (dayCheck == 0){
		popupText += "No contact days were selected.\n"
	}
	if (popupField == 1) {
		alert(popupText);
	}
}