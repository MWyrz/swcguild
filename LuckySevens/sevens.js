function play(){
	document.getElementById("playButton").innerHTML = "Play Again";
	document.getElementById("results").style.display = "block";
	var startingBet = document.getElementById("startingBet").value;
	var loopBet = document.getElementById("startingBet").value;
	var totalRolls = 0;
	var highestWon = 0;
	var highestRoll = 0;
	var rollOne, rollTwo;
	while(loopBet > 0){
		if(loopBet > highestWon){
			highestWon = loopBet;
			highestRoll = totalRolls;
		}
		rollOne = Math.floor((Math.random() * 6) + 1);
		rollTwo = Math.floor((Math.random() * 6) + 1);
		if(((rollOne*1) + (rollTwo*1))==7){
			loopBet = (loopBet*1) + (4*1);
		} 
		else{
			loopBet = (loopBet*1) - (1*1);
		}
		totalRolls = (totalRolls*1) + (1*1);
	}
	document.getElementById("resultBet").innerHTML = ("$" + startingBet);
	document.getElementById("resultTotalRolls").innerHTML = totalRolls;
	document.getElementById("resultWon").innerHTML = ("$" + highestWon);
	document.getElementById("resultWonRolls").innerHTML = highestRoll;
}